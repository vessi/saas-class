def palindrome?(string)
    str_a = string.downcase.gsub(/[a-z]/).to_a
    if str_a == str_a.reverse
        return true
    else
        return false
    end
end

def count_words(string)
    freqs = Hash.new(0);
    words_a = string.downcase.gsub(/\b[a-zA-Z]+\b/).to_a
    words_a.each { |word| freqs[word] += 1 }
    return freqs
end