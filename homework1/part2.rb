class WrongNumberOfPlayersError < StandardError ; end
class NoSuchStrategyError < StandardError ; end

def rps_game_winner(game)
    raise WrongNumberOfPlayersError unless game.length == 2
    
    winner1 = [['p','r'],['s','p'],['r','s']]
    winner2 = winner1.map { |pair| pair.reverse }
    pair = [game[0][1].downcase, game[1][1].downcase]
    if winner1.include? pair
        return game[0]
    elsif winner2.include? pair
        return game[1]
    elsif pair[0] == pair[1]
        return game[0]
    else
        raise NoSuchStrategyError
    end
end

def rps_tournament_winner(tournament)
    if tournament.flatten.count == 4
        return rps_game_winner(tournament)
    else
        winner1 = rps_tournament_winner(tournament[0])
        winner2 = rps_tournament_winner(tournament[1])
        return rps_tournament_winner([winner1, winner2])
    end
end

game = [["Armando", "S"], ["Dave", "P"]]
tournament1 = [["Armando","P"],["Dave","S"]]
tournament2 = [[[["Armando", "P"], ["Dave", "S"]],[["Richard", "R"],  ["Michael", "S"]],],[[["Allen", "S"], ["Omer", "P"]],[["David E.", "R"], ["Richard X.", "P"]]]]

rps_game_winner(game)
rps_tournament_winner(tournament1)
rps_tournament_winner(tournament2)