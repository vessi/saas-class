def combine_anagrams(words)
    hash = Hash.new(0)
    words.each do |word|
        chars = word.downcase.split(//).sort
        if hash.key? chars.join
            hash[chars.join] << word
        else
            hash.store(chars.join, [])
            hash[chars.join] << word
        end
    end
    hash.values.to_a
end

input = ['cars', 'for', 'potatoes', 'racs', 'four','scar', 'creams', 'scream'] 
combine_anagrams(input)