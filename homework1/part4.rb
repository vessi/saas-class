class Dessert
    attr_accessor :name, :calories

    def initialize(name, calories)
        @name = name
        @calories = calories
    end

    def healthy?
        return false unless calories < 200
        return true
    end

    def delicious?
        true
    end
end

class JellyBean < Dessert
    attr_accessor :flavor

    def initialize(name, calories, flavor)
        @flavor = flavor
    end

    def delicious?
        return true unless @flavor == 'black licorice'
        return false
    end
end